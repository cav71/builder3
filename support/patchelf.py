#!/usr/bin/env python3
import os
import sys
import struct
import argparse
import functools
import pathlib
import logging
import tempfile
import platform
import subprocess
import shlex

thisidr = pathlib.Path(__file__).parent
sys.path.insert(0, str(thisidr / "external.zip"))

import external.api as external

import click
import macholib.MachO

logger = logging.getLogger(__name__)

ALLOWED_SYSTEMS = [ "linux", "darwin", "win32", ]


def add_arguments(parser):
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-q", "--quiet", action="store_true")

    parser.add_argument("-n", "--dry-run", dest="dryrun", action="store_true")
    parser.add_argument("-f", "--force", action="store_true")

    parser.add_argument("--system", choices=ALLOWED_SYSTEMS,
       default=platform.system().lower())

    parser.add_argument("-A", "--absolute-rpath", action="store_true")
    parser.add_argument("--prefix")

    parser.add_argument("basedir")
    parser.add_argument("args", nargs="*")


def process_options(options):
    if options.system not in ALLOWED_SYSTEMS:
        parser.error("system [{}] not allowed".format(options.system))

    options.basedir = os.path.normpath(options.basedir)
    options.args = [ os.path.normpath(a) for a in options.args ]

    level = (
        logging.INFO
        if options.verbose  == options.quiet else
        logging.WARNING if options.quiet else logging.DEBUG
    )
    logging.basicConfig(level=level)
    delattr(options, "verbose")
    delattr(options, "quiet")
    options = [(o, getattr(options, o)) for o in dir(options) if not o.startswith("_")]
    return dict(options)


def identify(path, prefix):
    class Type:
        def __init__(self, typ="unknown", data=None):
            self.typ = typ or "unknown"
            self.data = data
        def __str__(self):
            if self.typ == "unknown":
                return f"T({self.typ} : {self.data})"
            else:
                return f"T({self.typ})"

    if os.path.islink(path):
        return Type("is-link")

    with open(path,"rb") as fp:
        data = fp.read()

    def check(*heads):
        for head in heads:
            txt = data[:len(head)]
            head = tuple(
                ord(h) if isinstance(h, str) else h
                for h in head
            )
            if head == tuple(data[:len(head)]):
                return True

    if check((0x7F, 'E', 'L', 'F')):
        return Type("elf")

    if check("#!/bin/sh", "#!/bin/bash"):
        return Type("sh")

    bprefix = prefix.encode("utf-8")
    if not bprefix in data:
        return Type("no-prefix")

    head = tuple(ord(c) for c in "#!")
    if head == tuple(data[:len(head)]):
        head = str(data[:data.index(b"\n")], encoding="utf-8")
        if prefix in head:
            return Type("py-script", head)
        return Type("shell", head)

    return Type(None, tuple(int(c) for c in data[:10]))


def walk(basedir, args):
    file_exclude = { ".pyc",}
    dir_exclude = { ".git", ".svn", "__pycache__" }
    def cleanpaths(dirnames, filenames):
        toremove = [ i for i, d in enumerate(dirnames)
            if d in dir_exclude
        ]
        for i in reversed(toremove):
            dirnames.pop(i)
        toremove = [ i for i, d in enumerate(dirnames)
            if os.path.splitext(d)[1] in file_exclude
        ]
        for i in reversed(toremove):
            filenames.pop(i)

    walkers = [
        os.walk(arg) if os.path.isdir(arg) else
        [(os.path.dirname(arg), [os.path.basename(arg),], [],), ]
        for arg in args 
    ]
    
    for walker in walkers:
        for root, dirnames, filenames in walker:
            cleanpaths(dirnames, filenames)  
            for filename in filenames:
                fullpath = os.path.join(root, filename)
                relpath = os.path.relpath(fullpath, basedir)
                yield fullpath, relpath


class Fixer:
    pass


class ELFFixer(Fixer):
    def __init__(self):
        self.exe = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "linux", "patchelf")
        )

    def patch(self, target, relpath, rpath=None):
        if not rpath:
            rpath = '$ORIGIN/' + os.sep.join([ '..' for n in range(str(relpath).count(os.sep)) ] + [ 'lib', ])
        cmd = [ self.exe, '--set-rpath', rpath, target, ]
        return [cmd,]

#class LinuxProcess(Processor):
#    def __init__(self, prefix):
#        super(LinuxProcess, self).__init__(prefix)
#
#    def setup(self):
#        self.exe = (pathlib.Path(__file__).parent / "linux" / "patchelf").absolute()
#        out = subprocess.check_output([self.exe, "--version"])
#
#    def can_patch(self, path):
#        if not super(LinuxProcess, self).can_patch(path):
#            return
#
#        with path.open("rb") as fp:
#            buf = fp.read(128)
#        fmt = "1b3s"
#        size = struct.calcsize(fmt)
#        if size > len(buf):
#            return False
#        ret = struct.unpack(fmt, buf[:size])
#        return (127, b"ELF") == ret 
#
#    def patch(self, target, relpath):
#        rpath = os.sep.join([ '..' for n in range(str(relpath).count(os.sep)) ] + [ 'lib', ])
#        cmd = [ self.exe,
#                '--set-rpath', '$ORIGIN/' + rpath, target, ]
#        return [cmd,]
#
#
#class DarwinProcess(Processor):
#    def setup(self):
#        self.exe = "install_name_tool"
#
#    def can_patch(self, path):
#        if not super(DarwinProcess, self).can_patch(path):
#            return
#        try:
#            m = macholib.MachO.MachO(path)
#            return True
#        except (ValueError, struct.error):
#            pass
#
#    def patch(self, target, relpath):
#        m = macholib.MachO.MachO(target)
#        
#        cmds = []
#        for cmd in m.headers[0].commands:
#            name = cmd[0].get_cmd_name()
#            if name in ("LC_LOAD_DYLIB",):
#                lib = cmd[2][:cmd[2].index(b"\0")].decode("utf-8")
#                if any(lib.startswith(e) for e in ("/usr/lib/", "/System/Library/", "@",)):
#                    continue
#                dst = pathlib.Path(lib) / relpath
#                replacement = pathlib.Path(relpath) / os.path.basename(lib)
#                cmds.append([
#                    self.exe, "-change", lib, f"@executable_path/{replacement}",
#                    target,                   
#                ])
#        return cmds
            

def execute(cmds):
    with tempfile.NamedTemporaryFile("w") as fp:
        print("# fix script", file=fp)
        for cmd in cmds:
            cmd = " ".join(shlex.quote(str(c)) for c in cmd)
            print(f"echo 'executing .. {cmd}' >&2", file=fp)
            print(cmd, file=fp)
        fp.flush()

        p = subprocess.Popen([ "sh", fp.name, ])
               # ,
               # stdout=subprocess.PIPE,
               # stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
    return p.returncode


def run(basedir, args, prefix, system, force, dryrun, absolute_rpath):
    logger.info("relocating files under %s", basedir)

    prefix = os.path.normpath(prefix or basedir)
    rpath = os.path.join(prefix, "lib") if absolute_rpath else None

    fixers = {}
    if system == "linux":
        fixers["elf"] = ELFFixer()
    else:
        raise NotImplementedError("system not supported", system)


    commands = []
    for fullpath, relpath in walk(basedir, args or [basedir,]):
        typ = identify(fullpath, prefix)
        logger.debug("detected %s for %s", typ, fullpath)

        if typ.typ in { "no-prefix", "is-link", "shell", "unknown", "py-script"}:
            continue
        actions = fixers.get(typ.typ, None)
        if not actions:
            continue
        if not isinstance(actions, (list, tuple)):
            actions = (actions,)
        for action in actions:
            commands.extend(action.patch(fullpath, relpath, rpath))

    if dryrun:
        print("Will execute..")
        for cmd in commands:
            cmd = " ".join(shlex.quote(str(c)) for c in cmd)
            print(" " + cmd)
    else:
        execute(commands)


def main():
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    options = parser.parse_args()
    run(**process_options(options))


if __name__ == "__main__":
    main()
