function dk-image
{
    docker build --squash -t py3 .
    dk-exec --rm --read-only py3 tree -L 3 /AC
}

function dk-pybuild
{
    local workdir
    [ $# -gt 1 ] && { echo "dk-pybuild <workingdir>" >&2; return 1; }
    [ $# -eq 0 ] && workdir="$(pwd)" || workdir="$1"
    mkdir -p $workdir/build/{tmp,exe,downloads}

    docker container run \
            --mount type=bind,src=$workdir,dst=/AC/builder3 \
            --mount type=bind,src=$workdir/build/tmp,dst=/AC/tmp \
            --mount type=bind,src=$workdir/build/exe,dst=/AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
            --mount type=bind,src=$workdir/build/downloads,dst=/AC/downloads \
        --rm --read-only py3 /bin/bash /AC/builder3/build.sh \
        /AC/tmp \
        /AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
        /AC/downloads
}

function dk-run
{
    dk-exec --rm -i py3 /bin/bash -i || true
}

function dk-sh
{
    local workdir
    [ $# -gt 1 ] && { echo "dk-pybuild <workingdir>" >&2; return 1; }
    [ $# -eq 0 ] && workdir="$(pwd)" || workdir="$1"
    mkdir -p $workdir/build/{tmp,exe,downloads}

    docker container rm py3shell || true
    docker container create \
            --mount type=bind,src=$workdir,dst=/AC/builder3 \
            --mount type=bind,src=$workdir/build/tmp,dst=/AC/tmp \
            --mount type=bind,src=$workdir/build/exe,dst=/AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
            --mount type=bind,src=$workdir/build/downloads,dst=/AC/downloads \
        -t -i --name py3shell py3 bash
    docker container start -a -i py3shell
}
