import sys

if __name__ == '__main__':
    with open(sys.argv[1], "r") as fp:
        inblock = False
        for line in fp:
            if "Python build finished successfully" in line:
                inblock = 1
                continue
            if "running build_scripts" in line:
                inblock = 0
                break
            if inblock:
                print(line.rstrip())
                
