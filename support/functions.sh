function npath
{
    # returns the absolute path of the joined args
    python -c 'import os, sys;\
               x = os.path.join(*sys.argv[1:]);\
               x = os.path.expanduser(x);\
               x = os.path.realpath(x);\
               x = os.path.normpath(x);\
               x = os.path.abspath(x);\
               print(x)' "$@" 
}
function isin
{
    #  checks if the first argument is in the remaining ones
    # isin d a b c d e -> ret code 0
    # isin x a b c d e -> ret code 1
    local val="$1"
    shift
    while [ $# -ne 0 ]
    do
        [ "$1" == "$val" ] && return 0 || true
        shift
    done
    return 1
}
function info  { echo "$(tput setaf 2)+$(tput sgr0) $@" >&2; }
function warn  { echo "$(tput setaf 3)!$(tput sgr0) $@" >&2; }
function error { echo "$(tput setaf 1)*$(tput sgr0) $@" >&2; exit 1;  }


function runsh {
    local dst="$1"
    local name="$2"
    local code="$3"
    local workdir="$( dirname "$1")"

    mkdir -p "$workdir" 

    cat >"$dst" <<@@
#!/bin/bash -e
set -e
name="$name"
workdir="$workdir"

system="$system"
basedir="$basedir"
builddir="$builddir"
downloads="$downloads"
prefix="$prefix"
selector="$system/$name"


function info  { echo "\$(tput setaf 2)+\$(tput sgr0) \$@" >&2; }
function warn  { echo "\$(tput setaf 3)!\$(tput sgr0) \$@" >&2; }
function error { echo "\$(tput setaf 1)*\$(tput sgr0) \$@" >&2; exit 1;  }

function uncompres
{
    local url="\$1"

    local ext
    local ecmd
    local wdir
    local tball="\$( basename "\$url")"
    case \$tball in
        *.tar.gz)
            ecmd="tar zxf"
            ext=.tar.gz
            ;;
        *.tgz)
            ecmd="tar zxf"
            ext=.tgz
            ;;
        *.tar.bz2)
            ecmd="tar jxf"
            ext=.tar.bz2
            ;;
        *.zip)
            ecmd="unzip"
            ext=.zip
            ;;
    esac       
    wdir="\$( basename "\$url" \$ext)"

    #echo \"got \$tball, \$ext, \$wdir, \$ecmd, \$url\"
    #exit 1
    if [ ! -f "\$downloads/\$tball" ]
    then
        mkdir -p "\$downloads"
        curlx -L "\$url" -o "\$downloads/\$tball"
    fi
    if [ ! -d "\$wdir" ]
    then
        \$ecmd "\$downloads/\$tball"
    fi
    cd "\$wdir"
}

function gnudance-make
{
    PKG_CONFIG_PATH="\$prefix/lib/pkgconfig" \
    make $MFLAGS \\
        2>>../log/buildlog.out >>../log/buildlog.err
    make install \\
        2>>../log/buildlog.out >>../log/buildlog.err
}

function gnudance-configure
{
    PKG_CONFIG_PATH="\$prefix/lib/pkgconfig" \
    ./configure "\$@" \\
        --prefix="\$prefix" \\
        --libdir="\$prefix/lib" \\
        2>../log/buildlog.out >../log/buildlog.err
}

function gnudance
{
    gnudance-configure "\$@"
    gnudance-make
}

cd "\$workdir"
mkdir -p log

export PATH="\$prefix/bin":\$PATH
export LD_LIBRARY_PATH="\$prefix/lib"

export CPPFLAGS="-I\\"\$prefix/include\\" -I\\"\$prefix/include/ncurses\\""
export LDFLAGS="-L\\"\$prefix/lib\\""


if [ "\$system" == "darwin" ]
then
    SDKDIR=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
    export CPPFLAGS="-I\\"\$SDKDIR/usr/include\\" \$CPPFLAGS"
    export LDFLAGS="-L\\"\$SDKDIR/usr/lib\\" \$LDFLAGS"
    function curlx { curl "\$@"; }
fi
if [ "\$system" == "linux" ]
then
    function curlx { LD_LIBRARY_PATH=/usr/lib64 /usr/bin/curl "\$@"; }
fi

$code
@@
    chmod a+rx "$workdir/run.sh"
}

function with
{
    local cache=yes
    [ "$1" == "--force-rebuild" ] && { cache=no; shift; } || cache=yes
    local name="$1" code="$2" 
    local selector="$system/$host/$name"

    local skip=no
    case $selector in
        darwin/*/patchelf)
            skip=yes
            ;;
    esac


    echo -n "$(tput sc)$(tput setaf 3)o$(tput sgr0) building $name ... " >&2
    [ "$cache" == "no" ] && rm -f "$builddir/$name/build.done" || true

    if [ -f "$builddir/$name/build.done" ]
    then
        echo "skipping, (to rebuild, remove "$builddir/$name/build.done")$(tput cr)$(tput setaf 2)=$(tput sgr0)" >&2
        return
    fi

    if [ "$skip" == "yes" ]
    then
        echo "skipping, not implemented for $system$(tput cr)$(tput setaf 2)=$(tput sgr0)" >&2
        return
    fi


    local t0=$( date +%s )
    mkdir -p "$builddir/$name/log"
    runsh "$builddir/$name/run.sh" "$name" "$code"
    "$builddir/$name/run.sh" \
        2>"$builddir/$name/log/stderr.log" >"$builddir/$name/log/stdout.log" \
        && { ret=0; } || { ret=1; }

    local delta=$(( $( date +%s ) - $t0 ))
    if [ $ret -eq 0 ]
    then
        echo "success (done in ${delta})s$(tput cr)$(tput setaf 2)+$(tput sgr0)" >&2
        echo "# built in ${delta}s" > "$builddir/$name/build.done"
        find "$prefix" >> "$builddir/$name/build.done"
    else
        echo "failed$(tput cr)$(tput setaf 1)*$(tput sgr0)" >&2
    fi
    return $ret

}

function with-check
{
    if [ 0${with_check_loglevel} -ne 0 ]
    then
        echo -n "$(tput setaf 2)+$(tput sgr0) $1 .. " >&2
        shift
    fi
    shift
    "$@" >& /dev/null
    local ret=$?   
    if [ 0${with_check_loglevel} -ne 0 ]
    then
        if [ $ret -ne 0 ]
        then
            echo "$(tput setaf 1)failed$(tput sgr0)" >&2
        else
            echo "$(tput setaf 2)success$(tput sgr0)" >&2
        fi
    fi
    return $ret
}

function fsetup-check-host
{
    # . support/functions.sh; fsetup-check-host --trace
    [ "$1" == "--trace" ] && { with_check_loglevel=1; shift; }
    local s
    [ -z "$1" ] && s="$( uname -s | tr A-Z a-z )" || s="$1"
    case $s in
        linux)
            with-check "checking readlink -f ." \
                readlink -f . || return
            if with-check "checkng redhat-release file"\
                test -f /etc/redhat-release
            then
                if [ ! -z "$( grep -E "8.[[:digit:]]+[.][[:digit:]]" /etc/redhat-release )" ]
                then
                    echo rhl8
                fi
            fi
        ;;
        darwin)
            echo macos
    esac
}

function fsetup
{
    export system="$( uname -s | tr A-Z a-z )"
    export host="$(fsetup-check-host $system)"

    export TERM=${TERM:=screen}
    export PATH="$(dirname "${BASH_SOURCE[1]}")/support/$system":$PATH
    export basedir="$( readlink -f "$(dirname "${BASH_SOURCE[1]}")" )"

}
