#!/usr/bin/env python3
import sys
import pathlib
import logging
import shutil

thisidr = pathlib.Path(__file__).parent
sys.path.insert(0, str(thisidr / "external.zip"))

import external.api as external

import click

logger = logging.getLogger(__name__)


def add_arguments(fn):
    return [
        click.option("-e", "--exe", default=sys.executable),
        click.option("-n", "--dry-run", "dryrun", default=None, is_flag=True),
        click.option("-f", "--force", default=False, is_flag=True),
        click.argument("sources", nargs=-1, type=pathlib.Path),
    ]


def fixfile(path):
    try:
        with path.open("rb") as fp:
            firstline = str(fp.read(2), encoding="utf-8", errors="replace")
            if firstline != "#!":
                return
            firstline += str(fp.readline(), encoding="utf-8", errors="replace")
        if "python" in firstline:
            return firstline, path
    except:
        logger.info("failed to process %s", path)


@external.cli(quiet=True)
def main(sources, exe, force, dryrun):
    tofix = []
    for source in sources:
        if source.is_file():
            path = fixfile(source)
            if path:
                tofix.append(path)
        else:
            for dirname, relpath in external.fs.walk(source):
                path = fixfile(dirname / relpath)
                if path:
                    tofix.append(path)

    exe = exe or "/usr/bin/env python3" 
    shebang = f"#!{exe}\n".encode('utf-8')
    for firstline, path in tofix:
        logger.info("processing %s", path)
        tmp = path.parent / (path.name + ".tmp")
        with tmp.open("wb") as fout:
            with path.open("rb") as fin:
                fin.read(len(firstline))
                fout.write(shebang)
                fout.write(fin.read())
        shutil.copymode(path, tmp)
        shutil.move(tmp, path)

if __name__ == "__main__":
    main()
