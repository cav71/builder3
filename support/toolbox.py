import re
import platform
import os, os.path
import operator
import fnmatch
import shutil
import textwrap
import argparse
import logging

import six


logger = logging.getLogger(__name__)


class ExceptionBase(Exception):
    pass

class FileNotFound(ExceptionBase):
    pass

class MultipleFilesFound(ExceptionBase):
    pass

class ExcecutionError(ExceptionBase):
    pass

class ObjectTyeError(ExceptionBase):
    pass

class TargetError(ExceptionBase):
    pass


def iswin():
    return 'WINDOWS' in platform.system().upper()



def islinux():
    return 'LINUX' in platform.system().upper()



def npath(*parts, **kwargs):
    """concatenates parts into a full path

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs-ext (str): replace the extension part from a path and replace it with ext
        kwargs-noabs (bool): don't apply the abspath to the result
        kwargs-realpath (bool): don't apply realpath (eg. don't resolve links)
        kwargs-ospath (module): uses ospath instead os.path for filename operations
        kwargs-encode (str): use a different encoding

    Returns:
        str: the concatenated parts

    Examples:
    # On a win32 system
    #    C:\\a\\b
    #  on unix
    #    /home/user/a/b
        >>> expected = os.path.join(os.getcwd(), 'a', 'b')
        >>> npath('a', 'b') == expected
        True
"""
    ext = kwargs.get('ext', None)
    noabs = kwargs.get('noabs', False)
    realpath = kwargs.get('realpath', False)
    ospath = kwargs.get('ospath', os.path)
    enc = kwargs.get('encode', None)

    if iswin():
        if len(parts) > 1:
            first = parts[0]
            if len(first) == 2 and re.match(r'\w:', first):
                first = '%s%s' % (parts[0], os.sep)
                parts = tuple([first] + list(parts[1:]))
    else:
        parts = [a.replace('\\', ospath.sep) for a in parts]

    x = ospath.join(*parts)
    x = ospath.expanduser(x)
    if not noabs:
        x = ospath.abspath(x)
    x = ospath.normpath(x)
    if not ext is None:
        multiexts = [
            '.tar.gz',
            '.tar.bz2',
        ]
        ext0 = endswith(x, multiexts)
        if ext0:
            x = x[:-len(ext0)] + ext
        else:
            x = x.rpartition('.')[0] + ext
    if realpath:
        x = ospath.realpath(x)
    return x if enc is None else x.encode(enc)


def basename(*parts, **kwargs):
    """returns the basename of concatenated parts

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str: the name part fo the concatenated path
    """
    return kwargs.get('ospath', os.path).basename(npath(*parts, **kwargs))


def dirname(*parts, **kwargs):
    """returns the dirname of concatenated parts

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str: the directory part fo the concatenated path
    """
    return kwargs.get('ospath', os.path).dirname(npath(*parts, **kwargs))


def relpath(path, start, **kwargs):
    """returns the relative path filename

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs-start: path to strip
        kwargs: same values as for npath

    Returns:
        str: the relative file name
    """
    return kwargs.get('ospath', os.path).relpath(path, start=start)


def exists(*parts, **kwargs):
    """returns a filename or None if the file doesn't exist

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or None: the file name or None if the file doesn't exist
    """
    target = npath(*parts, **kwargs)
    return target if kwargs.get('ospath', os.path).exists(target) else None


def isdir(*parts, **kwargs):
    """returns a filename or None if the file is a dir

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or False: the file name or False if the file is a dir
    """
    target = npath(*parts, **kwargs)
    return target if kwargs.get('ospath', os.path).isdir(target) else None


def isabs(*parts, **kwargs):
    """returns a filename or None if the file is absolute

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or False: the file name or False if the file is abs
    """
    kwargs['noabs'] = True
    target = npath(*parts, **kwargs)
    return target if kwargs.get('ospath', os.path).isabs(target) else None


def islink(*parts, **kwargs):
    """returns a filename or None if the file is a link

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or False: the file name or False if the file is link
    """
    kwargs['noabs'] = True
    target = npath(*parts, **kwargs)
    return target if kwargs.get('ospath', os.path).islink(target) else None



###########
# FILEOPS #
###########

def makedirs(*parts, **kwargs):
    """concatenates parts into a full path and creates the directory

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath
        kwargs-created: returns a list of created subdirs

    Returns:
        str or list of str: the directory name or the list of created subdiretories
"""
    try:
        klass, codes = WindowsError, [ 17, 183]
    except NameError:
        klass, codes = OSError, [ 17, 13, ]

    ospath = kwargs.get('ospath', os.path)
    return_created =  kwargs.pop('created') if 'created' in kwargs else False

    fullpath = npath(*parts, **kwargs)
    fullpath = fullpath.split(ospath.sep)

    created = []
    for i in range(1, len(fullpath)):
        try:
            sdir = ospath.sep.join(fullpath[:i+1])
            os.makedirs(sdir)
            created.append(sdir)
        except klass as e:
            if e.errno not in codes:
                raise
    return created if return_created else npath(*parts, **kwargs)


def rmtree(*parts, **kwargs):
    """concatenates parts into a full path and removes the file

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str: the target file name
"""
    target = npath(*parts, **kwargs)
    shutil.rmtree(target, ignore_errors=True, onerror=None)
    return target


def touch(*parts, **kwargs):
    """concatenates parts into a full path and creates the file

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or list of str: the full path to the created file
"""
    fullpath = npath(*parts, **kwargs)
    if isdir(fullpath):
        raise ObjectTyeError('found a dir as target', fullpath)
    makedirs(fullpath, '..')
    if not exists(fullpath):
        with open(fullpath, mode='w') as fp:
            pass
    return fullpath


def unlink(*parts, **kwargs):
    """concatenates parts into a full path and removes the file

    Args:
        parts (tuple of str):  strings to concatenate into an absolute path name
        kwargs: same values as for npath

    Returns:
        str or None: the file name or None if the file has not been removed
"""
    target = npath(*parts, **kwargs)
    try:
        klass, codes = (WindowsError, FileNotFoundError), [ 2, 17,]
    except NameError:
        klass, codes = OSError, [ 2,]
    try:
        os.unlink(target)
        return target
    except klass as e:
        if e.errno in codes:
            return
        raise


def which(exe, abort=True, allresults=False):
    """finds an executable

    Args:
        ext (str): executable to look for
        abort (bool or class): on True will raise FileNotFound on a failure
        allresults (bool): returns a list of candidates

    Returns:
        str or list: candidate(s)
    """
    candidates = []
    if isabs(exe):
        if exists(exe):
            candidates.append(exe)
    else:
        for subpath in os.environ.get('PATH', '').split(os.pathsep):
            if exists(subpath, exe):
                candidates.append(npath(subpath, exe))
            for ext in os.environ.get('PATHEXT', '').split(os.pathsep):
                if exists(subpath, exe + ext):
                    candidates.append(npath(subpath, exe + ext))

    if not candidates:
        if abort:
            raise (FileNotFound if abort is True else abort)('file {0} not found in PATH'.format(exe),
                                                             os.environ.get('PATH', ''))
        return [] if allresults else None
    return candidates if allresults else candidates[0]


######################
# CONDITIONAL BLOCKS #
######################


class TargetBase(object):
    class Skip(Exception):
        pass
    def __enter__(self):
        return self
    def __exit__(self, tb_type, tb_obj, tb_tbk):
        if tb_type is self.Skip:
            return True

    def __init__(self, name, c=None):
        self.name = name
        self.c = c or argparse.Namespace()

    def meet(self, abort=True):
        pass


class Target(TargetBase):
    def __init__(self, filenames, name=None, c=None, anyok=None):
        self.filenames = [ filenames, ] if isinstance(filenames, six.string_types) else filenames
        if len(self.filenames) != 1 and not name:
            raise TargetError('when filenames != 1 you need an explicit name')
        if not name:
            name = self.filenames[0]
        self.anyok = True if (anyok is None and len(self.filenames) == 1) else anyok
        super(Target, self).__init__(name=name, c=c)

    def meet(self, abort=True):
        existing = [ exists(x.format(c=self.c)) for x in self.filenames ]
        cond = any(existing) if self.anyok else all(existing)
        if cond and abort:
            raise self.Skip() 
        return cond



###################
# TEXT OPERATIONS #
###################

def indent(txt, indent=' '*3):
    '''dedents txt and then indents it with indent string

    Args:
        txt (str): a string to be indented
        indent (str): the indentation string

    Examples:

        >>> print(indent("""
        ...
        ...   Wow
        ...
        ...   This is a
        ... very complex text!
        ...
        ... """, indent='.'))
        .  Wow
        <BLANKLINE>
        .  This is a
        .very complex text!

    '''
    if not txt:
        return ''

    lines = linestrip(txt, sep='\n')
    result = textwrap.dedent(lines)
    return textwrap.indent(result, indent)


def endswith(txt, values, casesensitive=False, method='endswith'):
    values = [ values, ] if isinstance(values, six.string_types) else values
    found = None
    for value in values:
        if casesensitive:
            found = getattr(txt, method)(value)
        else:
            found = getattr(txt.upper(), method)(value.upper())
        if found:
            found = value
            break

    return found


def startswith(txt, values, casesensitive=False, method='startswith'):
    return endswith(txt, values, casesensitive=casesensitivee, method=method)


## CLI ##
