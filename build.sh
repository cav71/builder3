#!/bin/bash -e
[ -f "$(dirname ${BASH_SOURCE[0]})/support/functions.sh" ] &&\
    { source "$(dirname ${BASH_SOURCE[0]})/support/functions.sh"; fsetup; } ||\
    { echo "-> cannot find $(dirname ${BASH_SOURCE[0]})/support/functions.sh"; exit 1; }


[ $# -lt 2 ] && error "build <build-dir> <prefix> {<downloads-dir>} {<tmppath>}

settings
  system:    $system
  host:      ${host:=failed at detecting host, run: source support/functions.sh; fsetup-check-host --trace}
  basedir:   $basedir
"

mkdir -p "$1"
builddir="$( readlink -f "$1" )"
prefix="$( readlink -f "$2")"

[ $# -gt 2 ] && downloads="$( npath "$3")" || downloads="$builddir/downloads"
[ $# -gt 3 ] && export TMPDIR="$( npath "$4")" || export TMPDIR="$builddir"


info "system:    $system"    # this system (linux|darwin)
info "host:      $host"      # for a system check the class (eg. rhl)
info "basedir:   $basedir"   # the directory holding this script
info "builddir:  $builddir"  # the directory for builds
info "downloads: $downloads" # the directory for downloads
info "prefix:    $prefix"    # the prefix to install under
info "TMPDIR:    $TMPDIR"    # tmpdir


MFLAGS=-j5
with zlib "$(cat $basedir/recipes/zlib.sh)"
with openssl "$(cat $basedir/recipes/openssl.sh)"
with pigz "$(cat $basedir/recipes/pigz.sh)"
with sqlite "$(cat $basedir/recipes/sqlite.sh)"
with libffi "$(cat $basedir/recipes/libffi.sh)"
with libarchive "$(cat $basedir/recipes/libarchive.sh)"
with libxz "$(cat $basedir/recipes/libxz.sh)"
with bz2 "$(cat $basedir/recipes/bz2.sh)"
with python "$(cat $basedir/recipes/python.sh)"

exit 1
with --force-rebuild misc "$(cat $basedir/recipes/misc.sh)"

"$prefix/bin/python3" "$basedir/support/report.py" "$builddir/python/log/buildlog.err"



# Toolchain
#with gmp "$(cat $basedir/recipes/gmp.sh)"
#with mpfr "$(cat $basedir/recipes/mpfr.sh)"
#with mpc "$(cat $basedir/recipes/mpc.sh)"
#with gcc "$(cat $basedir/recipes/gcc.sh)"

# Support libs
#with ncurses "$(cat $basedir/recipes/ncurses.sh)"


info completed

