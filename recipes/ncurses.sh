# from: http://www.linuxfromscratch.org/lfs/view/development/chapter05/ncurses.html
uncompres https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz
    gnudance \
        --with-shared \
        --without-debug \
        --without-ada \
        --enable-widec \
        --enable-overwrite


