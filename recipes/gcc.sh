uncompres https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.gz
    gnudance \
        --enable-languages=c,c++,fortran \
        --disable-libstdcxx-pch \
        --disable-multilib \
        --disable-bootstrap \
        --with-gmp=\"\$prefix\" \
        --with-mpfr=\"\$prefix\" \
        --with-mpc=\"\$prefix\"

if [ -d "$prefix/lib64" ]
then
    mv "$prefix/lib64"/*.spec "$prefix/lib/" || true
    mv "$prefix/lib64"/*.{o,la,a,so}* "$prefix/lib" || true
    rmdir "$prefix/lib64"
fi
