case $selector in
    darwin/*)
        eflags="--disable-framework"
        ;;
    *)
        eflags=""
        ;;
esac

uncompres https://www.python.org/ftp/python/3.10.0/Python-3.10.0b1.tgz
    gnudance \
        $eflags --enable-shared --enable-optimizations --with-ensurepip=install

