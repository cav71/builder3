mkdir -p "$prefix/bin"
case $system in
    darwin)
        cp "$basedir/support/darwin/readlink" "$prefix/bin"
        exe="$( "$basedir/support/darwin/readlink" -f "$prefix/bin/python3")"
        ;;
    linux)
        cp "$basedir/support/linux/patchelf" "$prefix/bin"
        "$prefix/bin/patchelf" --set-rpath '$ORIGIN/../lib' "$( readlink -f "$prefix/bin/python3")"
        ;;
esac

# fix rpaths
"$prefix/bin/python3" "$basedir/support/patchelf.py" "$prefix"

#./support/linux/patchelf --set-rpath '$ORIGIN/../lib' /opt/python/cpython/3.9.5/bin/python3.9; /opt/python/cpython/3.9.5/bin/python3.9
