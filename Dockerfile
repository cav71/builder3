FROM centos:6

WORKDIR /

#RUN yum install -y yum-utils centos-release-scl m4
#RUN yum-config-manager --enable rhel-server-rhscl-6-rpms
#RUN yum install -y devtoolset-8

RUN rpm -i https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
RUN yum install -y yum-utils m4 tree gcc-c++ bash-completion

RUN mkdir -p /AC/{builder3,tmp,downloads,36624ceb-d6f3-420a-b5b1-f6d2dc4e8545} /opt/python

