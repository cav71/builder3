IMAGENAME=py3
BUILDDIR=$(PWD)/build/cross
MAGIC=36624ceb-d6f3-420a-b5b1-f6d2dc4e8545


help:
	@echo "make image"
	@echo 
	@echo "   image       - create a redhat image (centos 6)" 
	@echo "   build       - build the toolchain and the python interpreter"
	@echo "   shell       - launch a shell into the image (no-persist)"
	@echo "   test-shell      same but for testing"
	@echo "   test        - runs python tests"
	@echo "   external    - create the external.zip"
	@echo 
	@echo " > BUILDDIR: $(BUILDDIR)"

external:
	cd support/external; find . -type d -name __pycache__ -exec rm -rf "{}" \;
	cd support/external; find . -type f -name *pyc  -exec rm "{}" \;
	cd support/external; rm -f ../external.zip
	cd support/external; mkdir -p ../../build;  zip -qr ../../build/external.zip .


external-test: external
	PYTHONPATH=build/external.zip python3 -m external

image:
	docker build --squash -t $(IMAGENAME) .


build:
	mkdir -p $(BUILDDIR)/{tmp,exe,downloads}
	docker container run \
        --mount type=bind,src=$(PWD),dst=/AC/builder3 \
        --mount type=bind,src=$(BUILDDIR)/tmp,dst=/AC/tmp \
        --mount type=bind,src=$(BUILDDIR)/exe,dst=/AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
        --mount type=bind,src=$(BUILDDIR)/downloads,dst=/AC/downloads \
      --rm --read-only $(IMAGENAME) /bin/bash /AC/builder3/build.sh \
        /AC/tmp \
        /AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
        /AC/downloads

shell:
	docker container rm $(IMAGENAME)shell || true
	docker container create \
        --mount type=bind,src=$(PWD),dst=/AC/builder3 \
        --mount type=bind,src=$(BUILDDIR)/tmp,dst=/AC/tmp \
        --mount type=bind,src=$(BUILDDIR)/exe,dst=/AC/36624ceb-d6f3-420a-b5b1-f6d2dc4e8545 \
        --mount type=bind,src=$(BUILDDIR)/downloads,dst=/AC/downloads \
      -t -i --name $(IMAGENAME)shell $(IMAGENAME) bash
	docker container start -a -i $(IMAGENAME)shell


test-shell:
	docker container rm $(IMAGENAME)shell-test || true
	docker container create \
        --mount type=bind,readonly,src=$(BUILDDIR)/exe,dst=/opt/python \
      -t -i --name $(IMAGENAME)shell-test $(IMAGENAME) bash
	docker container start -a -i $(IMAGENAME)shell-test





.PHONY: image build shell shell-test external x
